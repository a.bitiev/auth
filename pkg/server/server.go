package server

import (
	"context"
	"errors"
	"net/http"
	"time"

	"github.com/sirupsen/logrus"
)

const (
	defaultAddr            = ":8080"
	defaultReadTimeout     = 300 * time.Millisecond
	defaultWriteTimeout    = 300 * time.Millisecond
	defaultShutdownTimeout = 3 * time.Second
)

type HttpServer struct {
	server          *http.Server
	logger          *logrus.Logger
	shutdownTimeout time.Duration
}

func NewHttpServer(handler http.Handler, logger *logrus.Logger, options ...Option) *HttpServer {
	s := &HttpServer{
		server: &http.Server{
			Addr:         defaultAddr,
			Handler:      handler,
			ReadTimeout:  defaultReadTimeout,
			WriteTimeout: defaultWriteTimeout,
		},
		logger:          logger,
		shutdownTimeout: defaultShutdownTimeout,
	}

	for _, applyOption := range options {
		applyOption(s)
	}

	return s
}

func (s *HttpServer) Run() {
	go func() {
		s.logger.Infof("staring http server at %s...", s.server.Addr)
		if err := s.server.ListenAndServe(); err != nil && !errors.Is(err, http.ErrServerClosed) {
			s.logger.Fatalf("listen and serve error: %v\n", err)
		}
	}()
}

func (s *HttpServer) Shutdown() error {
	s.logger.Infof("shutting down http server...")
	ctx, cancel := context.WithTimeout(context.Background(), s.shutdownTimeout)
	defer cancel()

	return s.server.Shutdown(ctx)
}
