package server

import "time"

type Option func(*HttpServer)

func WithAddr(addr string) Option {
	return func(s *HttpServer) {
		s.server.Addr = addr
	}
}

func WithReadTimeout(timeout time.Duration) Option {
	return func(s *HttpServer) {
		s.server.ReadTimeout = timeout
	}
}

func WithWriteTimeout(timeout time.Duration) Option {
	return func(s *HttpServer) {
		s.server.WriteTimeout = timeout
	}
}

func WithShutdownTimeout(timeout time.Duration) Option {
	return func(s *HttpServer) {
		s.shutdownTimeout = timeout
	}
}
