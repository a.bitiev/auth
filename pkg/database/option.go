package database

import "time"

type Option func(*PostgresDB)

func WithMaxConns(conns int) Option {
	return func(p *PostgresDB) {
		p.maxConns = conns
	}
}

func WithConnAttempts(attempts int) Option {
	return func(p *PostgresDB) {
		p.connAttempts = attempts
	}
}

func WithConnTimeout(timeout time.Duration) Option {
	return func(p *PostgresDB) {
		p.connTimeout = timeout
	}
}
