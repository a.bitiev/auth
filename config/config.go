package config

import (
	"fmt"
	"time"

	"github.com/ilyakaznacheev/cleanenv"
)

type Config struct {
	Server struct {
		Addr            string        `env-required:"true" env:"SERVER_ADDR"`
		ReadTimeout     time.Duration `env-required:"true" env:"SERVER_READ_TIMEOUT"`
		WriteTimeout    time.Duration `env-required:"true" env:"SERVER_WRITE_TIMEOUT"`
		ShutdownTimeout time.Duration `env-required:"true" env:"SERVER_SHUTDOWN_TIMEOUT"`
	}

	Database struct {
		URL          string        `env-required:"true" env:"DATABASE_URL"`
		MaxConns     int           `env-required:"true" env:"DATABASE_MAX_CONNS"`
		ConnAttempts int           `env-required:"true" env:"DATABASE_CONN_ATTEMPTS"`
		ConnTimeout  time.Duration `env-required:"true" env:"DATABASE_CONN_TIMEOUT"`
	}

	Hash struct {
		Salt string `env-required:"true" env:"HASH_SALT"`
	}
}

func NewConfig() (*Config, error) {
	cfg := &Config{}
	if err := cleanenv.ReadEnv(cfg); err != nil {
		return nil, fmt.Errorf("failed to read env variables: %w", err)
	}

	return cfg, nil
}
