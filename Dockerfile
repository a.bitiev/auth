FROM golang:1.19.2-alpine3.16 as builder
COPY go.mod go.sum /go/src/auth/
WORKDIR /go/src/auth
RUN go mod download
COPY . /go/src/auth
RUN CGO_ENABLED=0 GOOS=linux go build -a -o build/app /go/src/auth/cmd

FROM alpine
COPY --from=builder /go/src/auth/build/app /usr/bin/app
EXPOSE 8080
ENTRYPOINT ["/usr/bin/app"]