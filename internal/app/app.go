package app

import (
	"context"
	"os"
	"os/signal"

	"gitlab.com/a.bitiev/auth/config"
	v1 "gitlab.com/a.bitiev/auth/internal/controller/http/v1"
	"gitlab.com/a.bitiev/auth/internal/service"
	"gitlab.com/a.bitiev/auth/internal/storage"
	"gitlab.com/a.bitiev/auth/pkg/database"
	"gitlab.com/a.bitiev/auth/pkg/hash"
	"gitlab.com/a.bitiev/auth/pkg/server"

	"github.com/go-chi/chi"
	"github.com/sirupsen/logrus"
)

func Run(cfg *config.Config) {
	logger := logrus.New()
	logrus.SetLevel(logrus.InfoLevel)
	logrus.SetOutput(os.Stdout)

	ctx, stop := signal.NotifyContext(context.Background(), os.Interrupt)
	defer stop()

	database, err := database.NewPostgresDB(cfg.Database.URL, logger)
	if err != nil {
		logger.Fatalf("failed to create PostgresDB: %v", err)
	}
	defer database.Close()

	storage := storage.NewUserStorage(database)
	hasher := hash.NewSHA1Hasher(cfg.Hash.Salt)
	service := service.NewUserService(storage, hasher)

	router := chi.NewRouter()
	v1.InitRoutes(router, service, logger)

	server := server.NewHttpServer(router, logger)

	server.Run()

	<-ctx.Done()

	if err = server.Shutdown(); err != nil {
		logger.Errorf("shutdown error: %v", err)
	}
}
