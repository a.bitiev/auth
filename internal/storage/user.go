package storage

import (
	"context"

	"github.com/pkg/errors"

	"github.com/jackc/pgconn"
	"github.com/jackc/pgx/v4"
	"gitlab.com/a.bitiev/auth/internal/entity"
	"gitlab.com/a.bitiev/auth/internal/service"
	"gitlab.com/a.bitiev/auth/pkg/database"
)

type UserStorage struct {
	*database.PostgresDB
}

func NewUserStorage(db *database.PostgresDB) *UserStorage {
	return &UserStorage{
		PostgresDB: db,
	}
}

func (s *UserStorage) CreateUser(ctx context.Context, user *entity.User) (int, error) {
	var id int
	query := `INSERT INTO users (login, password, email, phone) VALUES ($1, $2, $3, $4) RETURNING id;`
	err := s.Pool.QueryRow(ctx, query, user.Login, user.Password, user.Email, user.Phone).Scan(&id)
	if err != nil {
		var pgError *pgconn.PgError
		if errors.As(err, &pgError) {
			if pgError.Code == "23505" {
				return 0, service.ErrUserAlreadyExists
			}
		}
		return 0, err
	}
	return id, nil
}

func (s *UserStorage) GetUser(ctx context.Context, login, password string) (*entity.User, error) {
	var user = &entity.User{}
	query := `SELECT * FROM users WHERE login = $1 AND password = $2;`
	row := s.Pool.QueryRow(ctx, query, login, password)
	if err := row.Scan(&user.Id, &user.Login, &user.Password, &user.Email, &user.Phone); err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return nil, service.ErrUserNotFound
		}
		return nil, err
	}

	return user, nil
}
