package v1

import (
	"context"
	"encoding/json"
	"errors"
	"io"
	"net/http"

	"github.com/go-chi/render"
	"github.com/sirupsen/logrus"
	"gitlab.com/a.bitiev/auth/internal/entity"
)

type UserService interface {
	Register(context.Context, *entity.User) (int, error)
	Login(context.Context, string, string) (*entity.User, error)
}

type Handler struct {
	service UserService
	logger  *logrus.Logger
}

func NewHandler(service UserService, logger *logrus.Logger) *Handler {
	return &Handler{
		service: service,
		logger:  logger,
	}
}

func (h *Handler) Register(w http.ResponseWriter, r *http.Request) {
	body, err := io.ReadAll(r.Body)
	defer r.Body.Close()
	if err != nil {
		render.Render(w, r, ServerErrorRenderer(err))
		h.logger.Error(err)
		return
	}

	var input RegisterRequest
	err = json.Unmarshal(body, &input)
	if err != nil {
		render.Render(w, r, ServerErrorRenderer(err))
		h.logger.Error(err)
		return
	}

	if err = input.Validate(); err != nil {
		render.Render(w, r, ErrorRenderer(err))
		h.logger.Error(err)
		return
	}

	id, err := h.service.Register(r.Context(), &entity.User{
		Login:    input.Login,
		Password: input.Password,
		Email:    input.Email,
		Phone:    input.Phone,
	})
	if err != nil {
		render.Render(w, r, ErrorRenderer(errors.Unwrap(err)))
		h.logger.Error(err)
		return
	}

	responce, err := json.Marshal(RegisterResponce{
		Id: id,
	})
	if err != nil {
		render.Render(w, r, ServerErrorRenderer(err))
		h.logger.Error(err)
		return
	}

	h.logger.Infof("created user with id = %d", id)

	w.WriteHeader(http.StatusCreated)
	w.Write([]byte(responce))
}

func (h *Handler) Login(w http.ResponseWriter, r *http.Request) {
	body, err := io.ReadAll(r.Body)
	defer r.Body.Close()
	if err != nil {
		render.Render(w, r, ServerErrorRenderer(err))
		h.logger.Error(err)
		return
	}

	var input LoginRequest
	err = json.Unmarshal(body, &input)
	if err != nil {
		render.Render(w, r, ServerErrorRenderer(err))
		h.logger.Error(err)
		return
	}

	if err = input.Validate(); err != nil {
		render.Render(w, r, ErrorRenderer(err))
		h.logger.Error(err)
		return
	}

	user, err := h.service.Login(r.Context(), input.Login, input.Password)
	if err != nil {
		render.Render(w, r, ErrorRenderer(errors.Unwrap(err)))
		h.logger.Error(err)
		return
	}

	responce, err := json.Marshal(LoginResponce{
		Email: user.Email,
		Phone: user.Phone,
	})

	if err != nil {
		render.Render(w, r, ServerErrorRenderer(err))
		h.logger.Error(err)
		return
	}

	h.logger.Infof("user with id = %d logged in", user.Id)

	w.WriteHeader(http.StatusOK)
	w.Write(responce)
}
