package v1

import (
	"net/http"

	"github.com/go-chi/render"
)

var (
	ErrBadRequest          = &ErrorResponse{StatusCode: http.StatusBadRequest, Message: "Bad request"}
	ErrNotFound            = &ErrorResponse{StatusCode: http.StatusNotFound, Message: "Resource not found"}
	ErrMethodNotAllowed    = &ErrorResponse{StatusCode: http.StatusMethodNotAllowed, Message: "Method not allowed"}
	ErrInternalServerError = &ErrorResponse{StatusCode: http.StatusInternalServerError, Message: "Internal server error"}
)

type ErrorResponse struct {
	Err        error  `json:"-"`
	StatusCode int    `json:"-"`
	Status     string `json:"status"`
	Message    string `json:"message"`
}

func (e *ErrorResponse) Render(w http.ResponseWriter, r *http.Request) error {
	render.Status(r, e.StatusCode)
	return nil
}

func ErrorRenderer(err error) *ErrorResponse {
	if err == nil {
		return ErrBadRequest
	}

	return &ErrorResponse{
		Err:        err,
		StatusCode: http.StatusBadRequest,
		Status:     "Bad request",
		Message:    err.Error(),
	}
}

func ServerErrorRenderer(err error) *ErrorResponse {
	return &ErrorResponse{
		Err:        err,
		StatusCode: http.StatusInternalServerError,
		Status:     "Internal server error",
		Message:    err.Error(),
	}
}
