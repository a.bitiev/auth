package v1

import (
	"fmt"
	"regexp"
)

type (
	RegisterRequest struct {
		Login    string `json:"login"`
		Password string `json:"password"`
		Email    string `json:"email"`
		Phone    string `json:"phone"`
	}

	RegisterResponce struct {
		Id int `json:"id"`
	}

	LoginRequest struct {
		Login    string `json:"login"`
		Password string `json:"password"`
	}

	LoginResponce struct {
		Email string `json:"email"`
		Phone string `json:"phone"`
	}
)

var (
	phone = regexp.MustCompile(`^(?:(?:\(?(?:00|\+)([1-4]\d\d|[1-9]\d?)\)?)?[\-\.\ \\\/]?)?((?:\(?\d{1,}\)?[\-\.\ \\\/]?){0,})(?:[\-\.\ \\\/]?(?:#|ext\.?|extension|x)[\-\.\ \\\/]?(\d+))?$`)
	email = regexp.MustCompile(`^[a-z0-9._%+\-]+@[a-z0-9.\-]+\.[a-z]{2,4}$`)
)

func (r *RegisterRequest) Validate() error {
	switch {
	case len(r.Login) < 6:
		return fmt.Errorf("login must be greater than 6 characters")
	case len(r.Password) < 6:
		return fmt.Errorf("password must be greater than 6 characters")
	case !phone.MatchString(r.Phone):
		return fmt.Errorf("invalid phone number")
	case !email.MatchString(r.Email):
		return fmt.Errorf("invalid email")
	}

	return nil
}

func (r *LoginRequest) Validate() error {
	switch {
	case len(r.Login) < 6:
		return fmt.Errorf("login must be greater than 6 characters")
	case len(r.Password) < 6:
		return fmt.Errorf("password must be greater than 6 characters")
	}

	return nil
}
