### Для запуска приложения:

```
docker-compose up
```

Если приложение запускается впервые, необходимо применить миграции к базе данных:

```
export POSTGRESQL_URL="postgres://admin:secret@localhost:5435/test_db?sslmode=disable"
migrate -database ${POSTGRESQL_URL} -path schema/migrations up
```